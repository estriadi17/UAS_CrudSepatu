package com.example.estri.sepatu.api;

import com.example.estri.sepatu.model.ResponseModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by estri on 08/06/2018.
 */

public interface RestApi {
    //insert
    @FormUrlEncoded
    @POST("insert.php")
    Call<ResponseModel> sendsepatu(@Field("nama_spt") String nama_spt, @Field("harga") String harga, @Field("sale") String sale,  @Field("diskon") String diskon,  @Field("warna") String warna);

    // read
    @GET("read.php")
    Call<ResponseModel> getsepatu();

    //update menggunakan 3 parameter
    @FormUrlEncoded
    @POST("update.php")
    Call<ResponseModel> updateData(@Field("id") String id, @Field("nama_spt") String nama_spt, @Field("harga") String harga, @Field("sale") String sale, @Field("diskon") String diskon, @Field("warna") String warna );

    // delete menggunakan parameter id
    @FormUrlEncoded
    @POST("delete.php")
    Call<ResponseModel> deleteData(@Field("id") String id);
}
