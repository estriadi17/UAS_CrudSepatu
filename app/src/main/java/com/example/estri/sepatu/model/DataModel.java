package com.example.estri.sepatu.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by estri on 08/06/2018.
 */

public class DataModel {
    @SerializedName("harga")
    private String mharga;
    @SerializedName("id")
    private String mId;
    @SerializedName("nama_spt")
    private String mnama_spt;
    @SerializedName("sale")
    private String msale;
    @SerializedName("diskon")
    private String mdiskon;
    @SerializedName("warna")
    private String mwarna;

    public String getharga() {
        return mharga;
    }

    public void setharga(String harga) {
        mharga = harga;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getnama_spt() {
        return mnama_spt;
    }

    public void setnama_spt(String nama_spt) {
        mnama_spt = nama_spt;
    }

    public String getsale() {
        return msale;
    }

    public void setsale(String sale) {
        msale = sale;
    }

    public String getdiskon() {
        return mdiskon;
    }

    public void setwarna(String warna) {
        mwarna = warna;
    }

    public String getwarna() {
        return mwarna;
    }

}
