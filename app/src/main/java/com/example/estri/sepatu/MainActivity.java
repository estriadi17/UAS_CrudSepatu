package com.example.estri.sepatu;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.estri.sepatu.api.RestApi;
import com.example.estri.sepatu.api.RetroServer;
import com.example.estri.sepatu.model.ResponseModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    EditText nama_spt, harga, sale, diskon, warna;
    Button btnsave, btnTampildata, btnupdate, btndelete;
    ProgressBar pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pd= (ProgressBar)findViewById(R.id.pd);
        pd.setIndeterminate(true);
        pd.setVisibility(View.GONE);
        nama_spt = (EditText) findViewById(R.id.edt_nama_spt);
        harga = (EditText) findViewById(R.id.edt_harga);
        sale = (EditText) findViewById(R.id.edtsale);
        diskon = (EditText) findViewById(R.id.edtdiskon);
        warna = (EditText) findViewById(R.id.edtwarna);
        btnTampildata = (Button) findViewById(R.id.btntampildata);
        btnupdate =(Button) findViewById(R.id.btnUpdate);
        btnsave = (Button) findViewById(R.id.btn_insertdata);
        btndelete=(Button) findViewById(R.id.btnhapus);
        //kondisi perubahan btn save > btn delete dan btn update
        Intent data = getIntent();
        final String iddata = data.getStringExtra("id");
        if(iddata != null) {
            btnsave.setVisibility(View.GONE);
            btnTampildata.setVisibility(View.GONE);
            btnupdate.setVisibility(View.VISIBLE);
            btndelete.setVisibility(View.VISIBLE);
            nama_spt.setText(data.getStringExtra("nama_spt"));
            harga.setText(data.getStringExtra("harga"));
            sale.setText(data.getStringExtra("sale"));
            diskon.setText(data.getStringExtra("diskon"));
            warna.setText(data.getStringExtra("warna"));
        }
        //btn update
        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setVisibility(View.VISIBLE);
                RestApi api =
                        RetroServer.getClient().create(RestApi.class);
                Call<ResponseModel> update =
                        api.updateData(iddata,nama_spt.getText().toString(),harga.getText().toString(),sale.getText().toString(),diskon.getText().toString(),warna.getText().toString());
                update.enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call,
                                           Response<ResponseModel> response) {
                        Log.d("Retro", "Response");
                        Toast.makeText(MainActivity.this,response.body().getPesan(),
                                Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(MainActivity.this,
                                TampilData.class));
                        pd.setVisibility(View.GONE);
                        finish();
                    }
                    @Override
                    public void onFailure(Call<ResponseModel> call,
                                          Throwable t) {
                        pd.setVisibility(View.GONE);
                        Log.d("Retro", "OnFailure");
                    }
                });
            }
        });
        //btn delete
        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setVisibility(View.VISIBLE);
                RestApi api =
                        RetroServer.getClient().create(RestApi.class);
                Call<ResponseModel> del = api.deleteData(iddata);
                del.enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call,
                                           Response<ResponseModel> response) {
                        pd.setVisibility(View.GONE);
                        Log.d("Retro", "onResponse");
                        Toast.makeText(MainActivity.this,
                                response.body().getPesan(),Toast.LENGTH_SHORT).show();
                        Intent gotampil = new
                                Intent(MainActivity.this,TampilData.class);
                        startActivity(gotampil);
                    }
                    @Override
                    public void onFailure(Call<ResponseModel> call,
                                          Throwable t) {
                        pd.setVisibility(View.GONE);
                        Log.d("Retro", "onFailure");
                    }
                });
            }
        });
        //btn tampil data
        btnTampildata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new
                        Intent(MainActivity.this,TampilData.class));
            }
        });
        //button insert
        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String snama_spt = nama_spt.getText().toString();
                String sharga = harga.getText().toString();
                String ssale = sale.getText().toString();
                String sdiskon = diskon.getText().toString();
                String swarna = warna.getText().toString();
                if (snama_spt.isEmpty() ) {
                    nama_spt.setError("nama sepatu perlu di isi");
                }else if (sharga.isEmpty()){
                    harga.setError("harga perlu di isi");
                }else if (ssale.isEmpty()) {
                    sale.setError("sale sepatu perlu di isi");
                }else if (sdiskon.isEmpty()) {
                    diskon.setError("diskon sepatu perlu di isi");
                }else if (swarna.isEmpty()) {
                    warna.setError("warna sepatu perlu di isi");

                } else {
                    RestApi api =
                            RetroServer.getClient().create(RestApi.class);
                    Call<ResponseModel> sendbio =
                            api.sendsepatu(snama_spt,sharga,ssale,sdiskon,swarna);
                    sendbio.enqueue(new Callback<ResponseModel>() {
                        @Override
                        public void onResponse(Call<ResponseModel> call,
                                               Response<ResponseModel> response) {
                            /*
                            pd.setVisibility(View.GONE);
                            */
                            Log.d("RETRO", "response : " +
                                    response.body().toString());
                            String kode = response.body().getKode();
                            if(kode.equals("1"))
                            {
                                Toast.makeText(MainActivity.this, "Data berhasil disimpan",
                                        Toast.LENGTH_SHORT).show();
                                startActivity(new
                                        Intent(MainActivity.this, TampilData.class));
                                nama_spt.getText().clear();
                                harga.getText().clear();
                                sale.getText().clear();
                                diskon.getText().clear();
                                warna.getText().clear();
                            }else
                            {
                                Toast.makeText(MainActivity.this, "Data Error tidak berhasil disimpan",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onFailure(Call<ResponseModel> call,
                                              Throwable t) {
                            /*
                            pd.setVisibility(View.GONE);
                            */
                            Log.d("RETRO", "Falure : " + "Gagal Mengirim Request");
                        }
                    });
                }}
        });
    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("warnig");
        alert.setMessage("do you wan to exit");
        alert.setPositiveButton("yes", new
                DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int
                            i) {
                        MainActivity.this.finish();
                    }
                });
        alert.setNegativeButton("no", new
                DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int
                            i) {
                    }
                });
        AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }
}
