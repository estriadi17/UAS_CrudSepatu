package com.example.estri.sepatu.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.estri.sepatu.MainActivity;
import com.example.estri.sepatu.R;
import com.example.estri.sepatu.model.DataModel;

import java.util.List;

/**
 * Created by estri on 08/06/2018.
 */

public class RecylerAdapter extends RecyclerView.Adapter<RecylerAdapter.MyHolder>{
    List<DataModel> mList ;
    Context ctx;

    public RecylerAdapter(Context ctx, List<DataModel> mList) {
        this.mList = mList;
        this.ctx = ctx;
    }
    @Override
    public RecylerAdapter.MyHolder onCreateViewHolder(ViewGroup
                                                              parent, int viewType) {
        View layout =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutlist,
                        parent, false);
        MyHolder holder = new MyHolder(layout);
        return holder;
    }
    @Override
    public void onBindViewHolder(RecylerAdapter.MyHolder holder,
                                 final int position) {
        holder.nama_spt.setText(mList.get(position).getnama_spt());
        holder.harga.setText(mList.get(position).getharga());
        holder.sale.setText(mList.get(position).getsale());
        holder.diskon.setText(mList.get(position).getdiskon());
        holder.warna.setText(mList.get(position).getwarna());
        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                Intent goInput = new Intent(ctx,MainActivity.class);
                try {
                    goInput.putExtra("id",
                            mList.get(position).getId());
                    goInput.putExtra("nama_spt",
                            mList.get(position).getnama_spt());
                    goInput.putExtra("harga",
                            mList.get(position).getharga());
                    goInput.putExtra("sale",
                            mList.get(position).getsale());
                    goInput.putExtra("diskon",
                            mList.get(position).getdiskon());
                    goInput.putExtra("warna",
                            mList.get(position).getwarna());
                    ctx.startActivity(goInput);
                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(ctx, "Error data " +e,
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    @Override
    public int getItemCount()
    {
        return mList.size();
    }
    public class MyHolder extends RecyclerView.ViewHolder {
        TextView nama_spt, harga, sale, diskon, warna;
        DataModel dataModel;
        public MyHolder(View v)
        {
            super(v);
            nama_spt = (TextView) v.findViewById(R.id.tvnama_spt);
            harga = (TextView) v.findViewById(R.id.tvharga);
            sale = (TextView) v.findViewById(R.id.tvsale);
            diskon = (TextView) v.findViewById(R.id.tvdiskon);
            warna = (TextView) v.findViewById(R.id.tvwarna);
        }
    }
}

