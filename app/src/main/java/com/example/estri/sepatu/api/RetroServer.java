package com.example.estri.sepatu.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by estri on 08/06/2018.
 */

public class RetroServer {
    //url tersimpannya file-file web service
    private static final String base_url = "http://192.168.43.38//sepatu/";

    private static Retrofit retrofit;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(base_url).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }
}